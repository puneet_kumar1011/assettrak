package asset.trak.networklayer

import asset.trak.database.entity.LastSyncObject
import asset.trak.model.AssetSyncRequestDataModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface AssetTrakAPIInterface {

    @GET("api/assetsync")
    suspend fun geLastSync(@QueryMap hashMap: Map<String, String>): Response<LastSyncObject>


    @POST("api/assetsync")
    suspend fun posAssetSync(@Body assetSyncRequestDataModel: AssetSyncRequestDataModel): Response<Any>
}