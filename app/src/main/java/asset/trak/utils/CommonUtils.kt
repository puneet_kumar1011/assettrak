package asset.trak.utils

import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.telephony.TelephonyManager
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

object Constants {
    var AssetClassId = 0
    var CategoryId="CategoryId"
    var SubCategoryId="SubCategoryId"
    var BookData="bookData"
    var AssetTitle="assetTitle"
    var CategoryTitle="categoryTitle"
    var SubCategoryTitle="subCategoryTitle"
    var LocationId=0
    var ScanId=0



    fun isInternetAvailable(context: Context): Boolean {
        var isConnected: Boolean = false // Initial Value
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }

    object InventoryStatus{
        const val PENDING="Pending"
        const val COMPLETED="Completed"
    }

    fun getIMEI(activity: Context): String? {
        val telephonyManager = activity
            .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return telephonyManager.deviceId
    }


    fun convertBitmapToFile(bitmap: Bitmap, context: Context,fileName:String): File? {
        try {
            // Create a file to write bitmap data
            val fileName = fileName+"/"+System.currentTimeMillis()
            var fos: OutputStream
            val file: File
            val imagesDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ).toString() + File.separator + "AssetTrack"

            file = File(imagesDir)

            if (!file.exists()) {
                file.mkdir()
            }

            val image = File(imagesDir, "$fileName.png")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val resolver: ContentResolver = context.getContentResolver()
                val contentValues = ContentValues()
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "Pictures/AssetTrack")
                val imageUri =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = resolver.openOutputStream(imageUri!!)!!

            } else {
                fos = FileOutputStream(image)
            }

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
            return image

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return null
    }

}