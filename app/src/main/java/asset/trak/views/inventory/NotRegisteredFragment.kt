package asset.trak.views.inventory

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import asset.trak.database.daoModel.BookAndAssetData
import asset.trak.database.entity.Inventorymaster
import asset.trak.database.entity.ScanTag
import asset.trak.database.entity.Selection
import asset.trak.views.adapter.NotFoundAdapter
import asset.trak.views.adapter.NotRegsiteredAdapter
import asset.trak.views.baseclasses.BaseFragment
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.application.Application.roomDatabaseBuilder
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces
import com.markss.rfidtemplate.inventory.InventoryListItem
import com.zebra.rfid.api3.RFIDResults
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_not_registered.*

@AndroidEntryPoint
class NotRegisteredFragment : BaseFragment(R.layout.fragment_not_registered) {
    private lateinit var notFoundAdapter: NotRegsiteredAdapter


     var rfidTags = ArrayList<ScanTag>()
    override fun onResume() {
        super.onResume()
        setAdaptor()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdaptor()
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                notFoundAdapter.filter.filter(newText)
                return true
            }
        })
    }

    private fun setAdaptor() {
        rfidTags.clear()
        val inventoryMasterList = bookDao.getInventoryMaster()
        val inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]
        rfidTags.addAll(bookDao?.getAssetNotRegistered(inventorymaster.scanID) ?: arrayListOf())

        notFoundAdapter = NotRegsiteredAdapter(requireContext(), rfidTags)
        rvRegistered.adapter = notFoundAdapter
    }

    fun updateList(){
        setAdaptor()
    }

}