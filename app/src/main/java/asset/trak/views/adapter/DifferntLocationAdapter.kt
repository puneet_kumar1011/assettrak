package asset.trak.views.adapter


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import asset.trak.database.daoModel.BookAndAssetData

import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.home.MainActivity
import com.markss.rfidtemplate.locate_tag.LocateOperationsFragment
import com.markss.rfidtemplate.rfid.RFIDController


class DifferntLocationAdapter(private val context: Context, private val fragment: FragmentManager,
                              private var items: ArrayList<BookAndAssetData>) :
    RecyclerView.Adapter<DifferntLocationAdapter.NotFoundHolder>(),Filterable {
    private var mFilteredList: List<BookAndAssetData>? = null

    inner class NotFoundHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvAuthor: TextView = view.findViewById(R.id.tvAuthor)
        var tvEdition: TextView = view.findViewById(R.id.tvEdition)
        var ivCheck: ImageView = view.findViewById(R.id.ivCheck)
        var clMain: ConstraintLayout = view.findViewById(R.id.clMain)
        var tvCategory: TextView = view.findViewById(R.id.tvCategory)
        var tvSearch: TextView = view.findViewById(R.id.tvSearch)


    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotFoundHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.itme_not_found, parent, false)
        return NotFoundHolder(itemView)
    }

    override fun onBindViewHolder(holder: NotFoundHolder, position: Int) {
        val item = items[position]
        holder.tvTitle.text = item.assetCatalogue?.assetName
        holder.tvAuthor.text = item.bookAttributes?.author
        holder.tvCategory.text = item.assetCatalogue.categoryName

        holder.tvEdition.text = "${context.getString(R.string.edition)} ${item.bookAttributes?.edition}"
        holder.tvSearch.visibility=View.VISIBLE
        holder.tvSearch.setOnClickListener{

            Application.locateTag = item.assetCatalogue.rfidTag
            RFIDController.accessControlTag = item.assetCatalogue.rfidTag
            Application.PreFilterTag = item.assetCatalogue.rfidTag

            replaceFragment(
                fragment, LocateOperationsFragment(),
                R.id.content_frame
            )

        }


        if (item.assetCatalogue.isSelected) holder.ivCheck.visibility=View.VISIBLE
        else holder.ivCheck.visibility=View.GONE


        holder.clMain.setOnClickListener {
            items[position].assetCatalogue.isSelected=!items[position].assetCatalogue.isSelected
            notifyDataSetChanged()

        }
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items.size

    }



    override fun getFilter(): Filter {
        return object : Filter() {

            @SuppressLint("DefaultLocale")
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val oReturn = FilterResults()
                val results = ArrayList<BookAndAssetData>()


                if (mFilteredList == null)
                    mFilteredList = items
                if (constraint != null) {
                    if (mFilteredList != null && mFilteredList!!.isNotEmpty()) {
                        for (mFilterData in mFilteredList!!) {

                            if (mFilterData.assetCatalogue.assetName?.lowercase()?.contains(constraint.toString().lowercase())==true
                                || mFilterData.bookAttributes?.author?.lowercase()?.contains(constraint.toString().lowercase())==true ||
                                mFilterData.assetCatalogue.rfidTag?.lowercase()?.contains(constraint.toString().lowercase())==true||
                                mFilterData.assetCatalogue.categoryName?.lowercase()?.contains(constraint.toString().lowercase())==true||
                                mFilterData.assetCatalogue.subCategoryName?.lowercase()?.contains(constraint.toString().lowercase())==true||
                                mFilterData.assetCatalogue.locationName?.lowercase()?.contains(constraint.toString().lowercase())==true||
                                mFilterData.assetCatalogue.searchTags?.lowercase()?.contains(constraint.toString().lowercase())==true||
                                mFilterData.bookAttributes?.publisher?.lowercase()?.contains(constraint.toString().lowercase())==true)

                                results.add(mFilterData)                            }
                    }
                    oReturn.count = results.size
                    oReturn.values = results
                } else {
                    oReturn.count = items.size
                    oReturn.values = items
                }
                return oReturn
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                items = (results.values as ArrayList<BookAndAssetData>)
                notifyDataSetChanged()
            }
        }
    }
    fun replaceFragment(fragmentManager: FragmentManager?, fragment: Fragment, id: Int) {
        fragmentManager?.beginTransaction()
            ?.replace(id, fragment, MainActivity.TAG_CONTENT_FRAGMENT)?.addToBackStack(null)?.commit()
    }
}