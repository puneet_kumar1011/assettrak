package asset.trak.views.module

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import asset.trak.database.entity.LastSyncData
import asset.trak.database.entity.LastSyncObject
import asset.trak.model.AssetSyncRequestDataModel
import asset.trak.repository.BookRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InventoryViewModel @Inject constructor(private val bookRepository: BookRepository) :
    ViewModel() {


    private var mLastSyncData = MutableLiveData<LastSyncData>()
    private var mAssetSyncData = MutableLiveData<Any>()

    fun getLastSync(): LiveData<LastSyncData> {
        viewModelScope.launch {
            val data = bookRepository.getLastSync()
            data?.apply {
                mLastSyncData.value = this.value
            }
        }
        return mLastSyncData
    }

    fun postAssetSync(asstSyncRequestData: AssetSyncRequestDataModel): LiveData<Any> {
        viewModelScope.launch {
            val data = bookRepository.postAssetSync(asstSyncRequestData)
            data?.apply {
                mAssetSyncData.value = this.value
            }
        }
        return mAssetSyncData
    }
}