package asset.trak.views.inventory

import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.widget.SearchView
import asset.trak.database.daoModel.BookAndAssetData
import asset.trak.views.adapter.DifferntLocationAdapter
import asset.trak.views.adapter.NotFoundAdapter
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.application.Application.roomDatabaseBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_different_loaction.*
import kotlinx.android.synthetic.main.fragment_different_loaction.searchView
import kotlinx.android.synthetic.main.fragment_my_library_search.*
import okhttp3.internal.notify

@AndroidEntryPoint
class DifferentLoactionFragment(val locationId: Int) :
    Fragment(R.layout.fragment_different_loaction) {
    private lateinit var notFoundAdapter: DifferntLocationAdapter

    var listBook = ArrayList<BookAndAssetData>()
    override fun onResume() {
        super.onResume()
        setAdaptor()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdaptor()
        searchView.queryHint = Html.fromHtml("<font color = #D3D3D3>" + getResources().getString(R.string.search) + "</font>");

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                notFoundAdapter.filter.filter(newText)
                return true
            }
        })
    }

    private fun setAdaptor() {
        listBook.clear()
        val inventoryMasterList = bookDao.getInventoryMaster()
        val inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]

        listBook.addAll(
            bookDao?.getAssetDifferentLoc(inventorymaster.scanID, locationId) ?: emptyList()
        )

        for (i in 0 until listBook.size)  {
            var locationName=Application.roomDatabaseBuilder?.getBookDao()?.getLocationName(listBook.get(i).assetCatalogue.locationId?:0)
            listBook[i].assetCatalogue?.locationName= locationName?.locationName?:""
            var category=Application.roomDatabaseBuilder?.getBookDao()?.getCatgeoryName(listBook.get(i).assetCatalogue.categoryId?:0)
            listBook[i].assetCatalogue?.categoryName= category?.categoryName?:""
            var subcategory=Application.roomDatabaseBuilder?.getBookDao()?.getSubCatgeoryName(listBook.get(i).assetCatalogue.subCategoryId?:0)
            listBook[i].assetCatalogue?.categoryName= subcategory?.subCategoryName?:""

        }
        notFoundAdapter = DifferntLocationAdapter(requireContext(), requireActivity().supportFragmentManager,listBook)

        rvNotFound.adapter = notFoundAdapter


    }

    fun updateList() {
       setAdaptor()
    }

}