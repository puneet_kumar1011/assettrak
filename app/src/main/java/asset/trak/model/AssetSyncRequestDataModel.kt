package asset.trak.model

import asset.trak.database.entity.Inventorymaster
import asset.trak.database.entity.ScanTag

class AssetSyncRequestDataModel {

    var assetScan: List<ScanTag>? = null
    var inventoryMaster: List<Inventorymaster>? = null
}


class AssetScanRequest {

    var rfidTag: String? = null
    var scanId: String? = null
    var newLocationId: Int = 0
}