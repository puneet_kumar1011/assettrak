package asset.trak.views.inventory

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import asset.trak.database.entity.Inventorymaster
import asset.trak.database.entity.LocationMaster
import asset.trak.utils.Constants
import asset.trak.views.baseclasses.BaseFragment
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application.roomDatabaseBuilder
import com.markss.rfidtemplate.rapidread.RapidReadFragment
import kotlinx.android.synthetic.main.fragment_view_inventory.*


class ViewInventoryFragment : BaseFragment(R.layout.fragment_view_inventory) {

    private var listOfLocations = ArrayList<LocationMaster>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialisation()
        setAdaptor()
        listeners()
    }

    private fun initialisation() {
        listOfLocations.clear()
        listOfLocations.addAll(roomDatabaseBuilder.getBookDao().getLocationMasterList())
    }

    private fun setAdaptor() {
        val listOfItems = ArrayList<String>()
        listOfItems.add(0,"Select Location")
        listOfLocations.forEach {
            listOfItems.add(it.locationName ?: "")
        }

        val spinnerArrayAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                listOfItems
            )
        spinnerLocation.adapter = spinnerArrayAdapter

    }

    private fun listeners() {
        ivBackButton.setOnClickListener {
            getBackToPreviousFragment()
        }

        spinnerLocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                tvRegisteredCount.text =
                    roomDatabaseBuilder.getBookDao().getCountRegisteredLastScan(
                        listOfLocations[position].id ?: 1
                    ).toString()
                tvNewlyScanCount.text = roomDatabaseBuilder.getBookDao().getCountNewlyScan(
                    listOfLocations[position].id ?: 1
                ).toString()
            }
        }

        /*    registered.setOnClickListener {
                replaceFragment(
                    requireActivity().supportFragmentManager, RecordInventoryNewFragment(),
                    R.id.content_frame
                )
            }*/

        buttonscan.setOnClickListener {
            /*Add a Entry to Table with Pending Status*/
            val getListInventoryMaster = roomDatabaseBuilder.getBookDao().getInventoryMaster()

            val inventoryLastItem: Inventorymaster = if (getListInventoryMaster.isNotEmpty())
                getListInventoryMaster[getListInventoryMaster.size - 1] else Inventorymaster()

            if (inventoryLastItem.status != Constants.InventoryStatus.PENDING) {
                val inventoryMaster=Inventorymaster(
                    scanID = "A" + ((inventoryLastItem.deviceIdCount ?: 0) + 1),
                    deviceId = "A",
                    deviceIdCount = ((inventoryLastItem.deviceIdCount ?: 0) + 1),
                    status = Constants.InventoryStatus.PENDING
                )

                roomDatabaseBuilder.getBookDao().addInventoryItem(inventoryMaster)
            }

            val fragment = RapidReadFragment()
            val bundle = Bundle()
            bundle.putParcelable(
                "LocationData",
                listOfLocations[spinnerLocation.selectedItemPosition]
            )
            fragment.arguments = bundle
            replaceFragment(
                requireActivity().supportFragmentManager, fragment,
                R.id.content_frame
            )
        }
    }


}