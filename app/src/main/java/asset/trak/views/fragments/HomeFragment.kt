package asset.trak.views.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import asset.trak.utils.Constants
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.inventory.InventoryRFragment
import asset.trak.views.module.InventoryViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.application.Application.roomDatabaseBuilder
import com.markss.rfidtemplate.settings.SettingListFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*
import java.io.File
import java.io.IOException
import java.net.URL
import java.net.URLConnection


private const val TAG = "HomeFragment"

@AndroidEntryPoint
class HomeFragment : BaseFragment(R.layout.fragment_home) {

    private var badgeBitmap: Bitmap? = null
    private var badgeURI: Uri? = null
    private var isImagedownloaded: Boolean = false
    private var badgeImage: File? = null

    private val inventoryViewModel: InventoryViewModel by viewModels()

    override fun onStart() {
        super.onStart()

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*Save Books Data to Database*/
//        saveDataToDataBase()
        listeners()

        if (Constants.isInternetAvailable(requireContext())) {
            getLastSync()
        }
          Log.e("dhdgdhdh","djd")


    }

    private fun listeners() {

        searchLin.setOnClickListener {
            replaceFragment(
                requireActivity().supportFragmentManager, AssetRegisterFragment(),
                R.id.content_frame
            )
        }
        linearScan.setOnClickListener {
            replaceFragment(
                requireActivity().supportFragmentManager, ScanFragment(),
                R.id.content_frame
            )
        }
        inventoryLin.setOnClickListener {

            replaceFragment(
                requireActivity().supportFragmentManager, InventoryRFragment(),
                R.id.content_frame
            )


        }
        configLin.setOnClickListener {

            replaceFragment(
                requireActivity().supportFragmentManager, SettingListFragment(),
                R.id.content_frame
            )


        }


    }


    /*  private fun saveDataToDataBase(){


  //
          val listOfAsset=ArrayList<AssetCatalogue>()

          listOfAsset.add(AssetCatalogue(1,"Against the Gods: The Remarkable Story of Risk","","",1,
              1, 1,1,"000000000000000000001258","",0,"","",0,R.drawable.againstthegods))

          listOfAsset.add(AssetCatalogue(2,"The Think & Grow Rich Action Pack","","",1,
              1, 1,1,"000000000000000000001271","",0,"","",0,R.drawable.book_law))


          listOfAsset.add(AssetCatalogue(3,"Material World: A Global Family Portrait","","",1,
              1, 1,1,"10011011002","",0,"","",0,R.drawable.confessions_of_an_economic_hit_man))


          listOfAsset.add(AssetCatalogue(4,"Don't Make Me Think: A Common Sense Approach to Web Usability","","",1,
              2, 3,1,"10011011003","",0,"","",0,R.drawable.dont_make_me_think))

          listOfAsset.add(AssetCatalogue(5,"The Visual Display of Quantitative Information","","",1,
              2, 4,1,"10011011004","",0,"","",0,R.drawable.handshake))

          listOfAsset.add(AssetCatalogue(6,"Confessions of an Economic Hit Man","","",1,
              1, 1,1,"10011011004","",0,"","",0,R.drawable.daring_greatly))
          listOfAsset.add(AssetCatalogue(7,"Start with Why: How Great Leaders Inspire Everyone to Take Action","","",1,
              2, 3,1,"10011011004","",0,"","",0,R.drawable.fast_food_nation))
          listOfAsset.add(AssetCatalogue(8,"Where the Sidewalk Ends","","",1,
              2, 3,1,"10011011004","",0,"","",0,R.drawable.fast_food_nation))

          listOfAsset.add(
              AssetCatalogue(9,"Moneyball: The Art of Winning an Unfair Game","","",1,
              2, 4,2,"10011011004","",0,"","",0,R.drawable.heart_of_buddha_teaching)
          )
          listOfAsset.add(AssetCatalogue(10,"The Creature from Jekyll Island: A Second Look at the Federal Reserve","","",1,
              2, 3,2,"10011011004","",0,"","",0,R.drawable.daring_greatly))


          bookDao?.addAssetCatalogueList(listOfAsset)

          val listOfBookAttributes=ArrayList<BookAttributes>()

          listOfBookAttributes.add(BookAttributes(1,"","Wiley","Peter L. Bernstein",
              "English",400,"1998","471295639","9780452266605"))
          listOfBookAttributes.add(BookAttributes(2,"","Penguin Publishing Group","Napoleon Hill",
              "English",352,"1998","452266602","9780452366605"))
          listOfBookAttributes.add(BookAttributes(3,"","Counterpoint Press","Peter Menzel, Charles C. Mann",
              "English",256,"1995","B007CZMFLI","'9780871564306"))
          listOfBookAttributes.add(BookAttributes(4,"","Penguin Publishing Group","John Perkins",
              "English",336,"2005","452287081","'9780452287082"))
          listOfBookAttributes.add(
              BookAttributes(5,"","Penguin Publishing Group"," Simon Sinek",
              "English",256,"2011","B008YF52PG","'9781591846444")
          )
          listOfBookAttributes.add(BookAttributes(6,"","Pearson Education"," Steve Krug",
              "English",216,"2005","321344758","'9780321344755"))
          listOfBookAttributes.add(BookAttributes(7,"","Graphics Press","Edward R. Tufte",
              "English",200,"2004","1930824130","'9781930824133"))
          listOfBookAttributes.add(BookAttributes(8,"","HarperCollins Publishers","Shel Silverstein",
              "English",192,"2004","B007C1RQKW","''9780060586539"))

          listOfBookAttributes.add(BookAttributes(9,"","Norton & Company, Incorporated, W. W.","Michael Lewis",
              "English",320,"2004","393324818","'''9780393324815"))

          listOfBookAttributes.add(BookAttributes(10,"","American Media","G. Edward Griffin",
              "English",320,"2004","60594896","'''9780393324815"))


          bookDao?.addBookAttributeList(listOfBookAttributes)








          *//*Assets*//*
        if(bookDao?.getAssetClassficationMasterList()?.isEmpty() ==true){
            val listOfAsset=ArrayList<AssetClassification>()

            listOfAsset.add(AssetClassification( id=1, className= "Library"))
            listOfAsset.add(AssetClassification( id=2, className= "Furniture"))
            listOfAsset.add(AssetClassification( id=3, className= "My Devices"))
            listOfAsset.add(AssetClassification( id=4, className= "My Jewellery"))
            bookDao?.addAssetClassfication(listOfAsset)
        }





        *//*Categories*//*
        if(bookDao?.getCategoriesMasterList()?.isEmpty() ==true) {
            val listOfCategories = ArrayList<CategoryMaster>()
            listOfCategories.add(CategoryMaster(id =1,categoryName="Art & Music",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =2,categoryName="Biographies",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =3,categoryName="Business",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =4,categoryName="Comics",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =5,categoryName="Computers & Technology",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =6,categoryName="Cooking",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =7,categoryName="Education & Reference",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =8,categoryName="Entertainment",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =9,categoryName="Health & Fitness",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =10,categoryName="History",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =11,categoryName="Hobbies & Crafts",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =12,categoryName="Home & Garden",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =13,categoryName="Horror",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =14,categoryName="Kids",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =15,categoryName="Literature & Fiction",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =16,categoryName="Medical",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =17,categoryName="Mysteries",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =18,categoryName="Parenting",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =19,categoryName="Religion",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =20,categoryName="Romance",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =21,categoryName="Sci-Fi & Fantasy",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =22,categoryName="Science & Math",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =23,categoryName="Self-Help",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =24,categoryName="Social Sciences",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =25,categoryName="Sports",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =26,categoryName="Teen",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =27,categoryName="Travel",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =28,categoryName="True Crime",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =29,categoryName="Westerns",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =30,categoryName="Single seat",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =31,categoryName="Multiple seats",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =32,categoryName="Sleeping or Lying",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =33,categoryName="Entertainment",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =34,categoryName="Tables",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =35,categoryName="Storage",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfCategories.add(CategoryMaster(id =36,categoryName="Sets",	SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))

            bookDao?.addCategoriesMasterList(listOfCategories)
        }

        *//*SubCategories*//*
        if(bookDao?.getSubCategoriesMasterList()?.isEmpty() ==true) {
            val listOfSubCategories = ArrayList<SubCategoryMaster>()
            listOfSubCategories.add(SubCategoryMaster(id = 1,subCategoryName="Art History",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 2,subCategoryName="Calligraphy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 3,subCategoryName="Drawing",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 4,subCategoryName="Fashion",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 5,subCategoryName="Film",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 6,subCategoryName="Ethnic & Cultural",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 7,subCategoryName="Europe",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 8,subCategoryName="Historical",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 9,subCategoryName="Leaders & Notable People",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 10,subCategoryName="Military",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 11,subCategoryName="Careers",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 12,subCategoryName="Economics",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 13,subCategoryName="Finance",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 14,subCategoryName="Industries",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 15,subCategoryName="International",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 16,subCategoryName="Comic Books",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 17,subCategoryName="Comic Strips",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 18,subCategoryName="Dark Horse",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 19,subCategoryName="DC Comics",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 20,subCategoryName="Fantasy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 21,subCategoryName="Apple",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 22,subCategoryName="CAD",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 23,subCategoryName="Certification",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 24,subCategoryName="Computer Science",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 25,subCategoryName="Databases",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 26,subCategoryName="Asian",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 27,subCategoryName="Baking",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 28,subCategoryName="BBQ",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 29,subCategoryName="Culinary Arts",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 30,subCategoryName="Desserts",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 31,subCategoryName="Almanacs & Yearbooks",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 32,subCategoryName="Atlases & Maps",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 33,subCategoryName="Catalogs",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 34,subCategoryName="Colleges",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 35,subCategoryName="Continuing Education",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 36,subCategoryName="Brain Teasers",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 37,subCategoryName="Entertainers",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 38,subCategoryName="Games",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 39,subCategoryName="Humor",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 40,subCategoryName="Movies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 41,subCategoryName="Aging",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 42,subCategoryName="Alternative Medicine",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 43,subCategoryName="Beauty, Grooming & Style",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 44,subCategoryName="Children's Health",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 45,subCategoryName="Disease & Ailments",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 46,subCategoryName="African",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 47,subCategoryName="Ancient",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 48,subCategoryName="Asian",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 49,subCategoryName="Black History",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 50,subCategoryName="Canadian",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 51,subCategoryName="Antiques",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 52,subCategoryName="Clay",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 53,subCategoryName="Collecting",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 54,subCategoryName="Fashion",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 55,subCategoryName="Jewelry Making",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 56,subCategoryName="Architecture",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 57,subCategoryName="Flowers",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 58,subCategoryName="Fruit",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 59,subCategoryName="Home Decorating",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 60,subCategoryName="Home Improvement",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 61,subCategoryName="Ghosts",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 62,subCategoryName="Paranormal",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 63,subCategoryName="Supernatural",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 64,subCategoryName="Vampires",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 65,subCategoryName="Zombies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 66,subCategoryName="Action & Adventure",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 67,subCategoryName="Activities, Crafts & Games",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 68,subCategoryName="Activity Books",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 69,subCategoryName="Animals",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 70,subCategoryName="Anthologies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 71,subCategoryName="Classics",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 72,subCategoryName="Contemporary",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 73,subCategoryName="Foreign Language",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 74,subCategoryName="Genre Fiction",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 75,subCategoryName="Administration",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 76,subCategoryName="Allied Health",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 77,subCategoryName="Basic Sciences",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 78,subCategoryName="Clinical",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 79,subCategoryName="Dentistry",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 80,subCategoryName="Conspiracy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 81,subCategoryName="Crime",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 82,subCategoryName="Detective",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 83,subCategoryName="Mysteries",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 84,subCategoryName="Suspense",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 85,subCategoryName="Adoption",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 86,subCategoryName="Aging Parents",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 87,subCategoryName="Child Care",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 88,subCategoryName="Family Activities",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 89,subCategoryName="Family Health",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 90,subCategoryName="Agnosticism",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 91,subCategoryName="Astrology",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 92,subCategoryName="Atheism",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 93,subCategoryName="Buddhism",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 94,subCategoryName="Christian",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 95,subCategoryName="Anthologies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 96,subCategoryName="Contemporary",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 97,subCategoryName="Erotica",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 98,subCategoryName="Fantasy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 99,subCategoryName="Gothic",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 100,subCategoryName="Action",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 101,subCategoryName="Anthologies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 102,subCategoryName="Coming of Age",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 103,subCategoryName="Dark Fantasy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 104,subCategoryName="Fantasy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 105,subCategoryName="Agricultural Sciences",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 106,subCategoryName="Anatomy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 107,subCategoryName="Animals",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 108,subCategoryName="Astronomy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 109,subCategoryName="Biology",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 110,subCategoryName="Abuse",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 111,subCategoryName="Addictions",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 112,subCategoryName="Anger Management",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 113,subCategoryName="Anxieties & Phobias",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 114,subCategoryName="Creativity",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 115,subCategoryName="Anarchy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 116,subCategoryName="Canadian Politics",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 117,subCategoryName="Civil Rights",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 118,subCategoryName="Comparative Politics",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 119,subCategoryName="Cultural",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 120,subCategoryName="Baseball & Softball",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 121,subCategoryName="Basketball",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 122,subCategoryName="Boating & Sailing",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 123,subCategoryName="Camping",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 124,subCategoryName="Extreme Sports",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 125,subCategoryName="Being a Teen",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 126,subCategoryName="Fantasy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 127,subCategoryName="Historical Fiction",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 128,subCategoryName="Hobbies",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 129,subCategoryName="Horror",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 130,subCategoryName="Africa",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 131,subCategoryName="Asia",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 132,subCategoryName="Canada",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 133,subCategoryName="Caribbean",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 134,subCategoryName="Europe",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 135,subCategoryName="Criminal Law",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 136,subCategoryName="Famous Criminals",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 137,subCategoryName="Murder & Mayhem",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 138,subCategoryName="Organized Crime",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 139,subCategoryName="Serial Killers",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 140,subCategoryName="Native American",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 141,subCategoryName="Chair",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 142,subCategoryName="Lift chair",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 143,subCategoryName="Bean bag",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 144,subCategoryName="Chaise longue",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 145,subCategoryName="Fauteuil",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 146,subCategoryName="Ottoman",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 147,subCategoryName="Recliner",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 148,subCategoryName="Stool",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 149,subCategoryName="Bar Stool",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 150,subCategoryName="Footstool or ottoman",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 151,subCategoryName="Tuffet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 152,subCategoryName="Fainting couch",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 153,subCategoryName="Rocking chair",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 154,subCategoryName="Bar chair",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 155,subCategoryName="Poufy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 156,subCategoryName="Arm Chair",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 157,subCategoryName="Bench",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 158,subCategoryName="Couch",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 159,subCategoryName="Couch-Accubita",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 160,subCategoryName="Couch-Canape",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 161,subCategoryName="Couch-Davenport",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 162,subCategoryName="Couch-Klinai",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 163,subCategoryName="Divan",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 164,subCategoryName="Love seat",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 165,subCategoryName="Chesterfield",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 166,subCategoryName="Divan",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 167,subCategoryName="Love seat",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 168,subCategoryName="Chesterfield",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 169,subCategoryName="Bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 170,subCategoryName="Bunk bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 171,subCategoryName="Canopy bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 172,subCategoryName="Four-poster bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 173,subCategoryName="Murphy bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 174,subCategoryName="Platform bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 175,subCategoryName="Sleigh bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 176,subCategoryName="Waterbed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 177,subCategoryName="Daybed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 178,subCategoryName="Futon",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 179,subCategoryName="Hammock",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 180,subCategoryName="Headboard",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 181,subCategoryName="Infant bed (crib, cradle)",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 182,subCategoryName="Sofa bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 183,subCategoryName="Toddler bed",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 184,subCategoryName="Billiard table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 185,subCategoryName="Chess table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 186,subCategoryName="Entertainment center",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 187,subCategoryName="Gramophone",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 188,subCategoryName="Hi-fi",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 189,subCategoryName="Jukebox",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 190,subCategoryName="Pinball machine",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 191,subCategoryName="Radiogram",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 192,subCategoryName="Home bar",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 193,subCategoryName="Radio receiver",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 194,subCategoryName="Piano",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 195,subCategoryName="Countertop",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 196,subCategoryName="Chabudai",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 197,subCategoryName="Changing table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 198,subCategoryName="Desk",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 199,subCategoryName="Davenport desk",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 200,subCategoryName="Drawing board",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 201,subCategoryName="Computer desk",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 202,subCategoryName="Writing desk",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 203,subCategoryName="Kotatsu",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 204,subCategoryName="Korsi",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 205,subCategoryName="Lowboy",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 206,subCategoryName="Monks bench",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 207,subCategoryName="Pedestal",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 208,subCategoryName="Table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 209,subCategoryName="Game table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 210,subCategoryName="Coffee table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 211,subCategoryName="Dining table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 212,subCategoryName="Refectory table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 213,subCategoryName="Drop-leaf table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 214,subCategoryName="End table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 215,subCategoryName="Folding table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 216,subCategoryName="Gateleg table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 217,subCategoryName="Poker table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 218,subCategoryName="Trestle table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 219,subCategoryName="TV tray table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 220,subCategoryName="Wine table",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 221,subCategoryName="Washstand",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 222,subCategoryName="Workbench",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 223,subCategoryName="Bakers rack",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 224,subCategoryName="Bookcase",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 225,subCategoryName="Cabinetry",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 226,subCategoryName="Bathroom cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 227,subCategoryName="Chifforobe",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 228,subCategoryName="Closet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 229,subCategoryName="Cupboard",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 230,subCategoryName="Curio cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 231,subCategoryName="Gun cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 232,subCategoryName="Hutch",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 233,subCategoryName="Hoosier cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 234,subCategoryName="Kitchen cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 235,subCategoryName="Liquor cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 236,subCategoryName="Pantry",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 237,subCategoryName="Pie safe",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 238,subCategoryName="Stipo a bambocci",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 239,subCategoryName="Chest of drawers or dresser",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 240,subCategoryName="Chest",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 241,subCategoryName="Cellarette",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 242,subCategoryName="Hope chest",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 243,subCategoryName="Coat rack",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 244,subCategoryName="Drawer (furniture)",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 245,subCategoryName="Hatstand",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 246,subCategoryName="Bar cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 247,subCategoryName="Filing cabinet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 248,subCategoryName="Floating shelf",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 249,subCategoryName="Nightstand",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 250,subCategoryName="Ottoman",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 251,subCategoryName="Plan chest",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 252,subCategoryName="Plant stand",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 253,subCategoryName="Shelving",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 254,subCategoryName="Sideboard or buffet",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 255,subCategoryName="Umbrella stand",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 256,subCategoryName="Bedroom set (group)",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 257,subCategoryName="Dinette (group)",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 258,subCategoryName="Dining set",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 259,subCategoryName="Vanity set",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 260,subCategoryName="Portable Lamps",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))
            listOfSubCategories.add(SubCategoryMaster(id = 261,subCategoryName="Patio set",SyncFlag = 0,CreatedOn = "2022-02-03 07:00:07",ModifiedOn = ""))

            bookDao?.addSubCategoriesMasterList(listOfSubCategories)
        }

        *//*Categories*//*
        if(bookDao?.getLocationMasterList()?.isEmpty() ==true) {
            val listOfLocation = ArrayList<LocationMaster>()
            listOfLocation.add(LocationMaster(id = 1, locationName = "Upper Shelf", rfidTag = "111",
                SyncFlag = 1, CreatedOn = "2022-02-03 07:00:07"))
            listOfLocation.add(LocationMaster(id = 2, locationName = "Middle Shelf", rfidTag = "222",
                SyncFlag = 1, CreatedOn = "2022-02-03 07:00:07"))
            listOfLocation.add(LocationMaster(id = 3, locationName = "Lower Shelf", rfidTag = "333",
                SyncFlag = 1, CreatedOn = "2022-02-03 07:00:07"))

            bookDao?.addLocationMasterList(listOfLocation)
        }
        if(bookDao?.getAssetClassMapList()?.isEmpty()==true) {
            val listOfAssetClassMAp = ArrayList<AssetClassCatMap>()
            listOfAssetClassMAp.add(AssetClassCatMap(1,1,1))
            listOfAssetClassMAp.add(AssetClassCatMap(2,1,2))
            listOfAssetClassMAp.add(AssetClassCatMap(3,1,3))
            listOfAssetClassMAp.add(AssetClassCatMap(4,1,4))
            listOfAssetClassMAp.add(AssetClassCatMap(5,3,5))
            listOfAssetClassMAp.add(AssetClassCatMap(6,3,6))
            listOfAssetClassMAp.add(AssetClassCatMap(7,4,7))
            listOfAssetClassMAp.add(AssetClassCatMap(8,4,8))

            bookDao?.addAssetClassCatMap(listOfAssetClassMAp)
        }
        if(bookDao?.getCatSubMapList()?.isEmpty()==true) {
            val listofCatSubmap = ArrayList<CatSubCatMap>()
            listofCatSubmap.add(CatSubCatMap(1,1,1))
            listofCatSubmap.add(CatSubCatMap(2,1,2))
            listofCatSubmap.add(CatSubCatMap(3,2,3))
            listofCatSubmap.add(CatSubCatMap(4,2,4))
            listofCatSubmap.add(CatSubCatMap(5,3,5))
            listofCatSubmap.add(CatSubCatMap(6,3,6))
            listofCatSubmap.add(CatSubCatMap(7,4,7))
            listofCatSubmap.add(CatSubCatMap(8,4,8))

            bookDao?.addCatSubClassification(listofCatSubmap)
        }

    }*/


    private fun  getLastSync() {
        inventoryViewModel.getLastSync().observe(viewLifecycleOwner) {

//            if (it != null && it.statusCode == 200) {

            if (!it.assetClassification.isNullOrEmpty()) {
                bookDao?.addAssetClassfication(it.assetClassification!!)
            }

            if (!it.categoryMaster.isNullOrEmpty()) {
                bookDao?.addCategoriesMasterList(it.categoryMaster!!)
            }

            if (!it.assetClassCategoryMap.isNullOrEmpty()) {
                bookDao?.addAssetClassCatMap(it.assetClassCategoryMap!!)
            }


            if (!it.subCategoryMaster.isNullOrEmpty()) {
                bookDao?.addSubCategoriesMasterList(it.subCategoryMaster!!)
            }

            if (!it.categorySubCategoryMap.isNullOrEmpty()) {
                bookDao?.addCatSubClassification(it.categorySubCategoryMap!!)
            }


            if (!it.locationMaster.isNullOrEmpty()) {
                bookDao?.addLocationMasterList(it.locationMaster!!)
            }


            if (!it.assetCatalogue.isNullOrEmpty()) {
                it.assetCatalogue!!.forEach {
                   it.imagePathFile= downloadImage(it.imageUrl!!,it.imageId!!)

                }
                bookDao?.addAssetCatalogueList(it.assetCatalogue!!)

            }


            if (!it.bookAttributes.isNullOrEmpty()) {
                bookDao?.addBookAttributeList(it.bookAttributes!!)

            }


        }




    }


    fun downloadImage(imageUrl:String,fileName: String):String{
        var savePath: String?=null
        Glide.with(this)
            .asBitmap()
            .load(imageUrl)
            .into(object : CustomTarget<Bitmap>(){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {

                    badgeBitmap=resource
                    val savedUri = saveToInternalStorage(requireContext(),fileName)

                    roomDatabaseBuilder.getBookDao().getBooks().listIterator().forEach {
                        if(it.assetCatalogue.imagePathFile?.isNotEmpty() == true)
                        it.assetCatalogue.imagePathFile=savedUri.toString()
                    }
                    savePath=savedUri?.path
                }
                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })

        return savePath?:""
    }

    private fun DownloadImageFromPath(path: String?,fileName: String) {
        Log.d("img==",path.toString())

        val urlImage = URL(path.toString())
        val result: Deferred<Bitmap?> = GlobalScope.async {
            urlImage.toBitmap()
        }

        GlobalScope.launch(Dispatchers.Main) {
            val bitmap: Bitmap? = result.await()
            bitmap?.apply {
                badgeBitmap = this
                val savedUri: Uri? = saveToInternalStorage(requireContext(),fileName)
//                roomDatabaseBuilder.
                Log.d("uri==", savedUri.toString())
            }
        }


    }

    // extension function to get / download bitmap from url
    private fun URL.toBitmap(): Bitmap? {
        return try {
            BitmapFactory.decodeStream(openStream())
        } catch (e: IOException) {
            null
        }
    }

    // extension function to save an image to internal storage
    private fun saveToInternalStorage(context: Context,fileName:String): Uri? {
        return try {
            val file: File = Constants.convertBitmapToFile(badgeBitmap!!, requireContext(),fileName)!!
            badgeImage = file
            isImagedownloaded = true
            badgeURI = Uri.parse(file.absolutePath)
            Uri.parse(file.absolutePath)

        } catch (e: Exception) { // catch the exception
            e.printStackTrace()
            null
        }
    }

}
