package asset.trak.views.inventory

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import asset.trak.database.entity.AssetCatalogue
import asset.trak.database.entity.LocationMaster
import asset.trak.database.entity.ScanTag
import asset.trak.views.adapter.ReconcileAssetsPagerAdapter
import asset.trak.views.baseclasses.BaseFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces
import com.markss.rfidtemplate.home.MainActivity
import com.markss.rfidtemplate.inventory.InventoryListItem
import com.markss.rfidtemplate.locate_tag.LocateOperationsFragment
import com.markss.rfidtemplate.modules.login.activity.LoginActivity
import com.markss.rfidtemplate.rfid.RFIDController
import com.markss.rfidtemplate.utils.PreferenceUtil
import com.zebra.rfid.api3.RFIDResults
import kotlinx.android.synthetic.main.fragment_reconcile_assets.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class ReconcileAssetsFragment : BaseFragment(R.layout.fragment_reconcile_assets),
    ResponseHandlerInterfaces.ResponseTagHandler, ResponseHandlerInterfaces.TriggerEventHandler,
    ResponseHandlerInterfaces.ResponseStatusHandler {


    private lateinit var adapter: ReconcileAssetsPagerAdapter

    var locationId = 0
    val listInventoryList = HashSet<String>()


    override fun handleTagResponse(inventoryListItem: InventoryListItem?, isAddedToList: Boolean) {
        listInventoryList.clear()
        listInventoryList.add(inventoryListItem?.tagID ?: "")
        addScan()

        if (isAddedToList) {
            Log.d("test", "testresponse${isAddedToList}")
            Log.d("test", "testresponse${inventoryListItem!!.tagID}")
            if (!Application.TAG_LIST_MATCH_MODE) {

            }
        }

    }


    override fun handleStatusResponse(results: RFIDResults?) {

    }


    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {

//            lifecycleScope.launch {
//                val activity = activity as MainActivity?
//                activity?.inventoryStartOrStop()
//            }
        }
    }

    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            //RFIDController.mInventoryStartPending = false;
            lifecycleScope.launch {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBackButton.setOnClickListener {
            getBackToPreviousFragment()
        }
        locationId = arguments?.getInt("locationId") ?: 0
        val locationData = arguments?.getParcelable<LocationMaster>("LocationData")

        tvFloorTitle.text = locationData?.locationName
        var locationRegsiterCount = bookDao.getCountLocationId(locationId)
        val totalcount = "Total Registered Assets : $locationRegsiterCount"

        tvTotalRegisteredAssets.text = totalcount


        val inventoryMasterList = bookDao.getInventoryMaster()
        val inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]


        var notFoundCount = bookDao.getCountOfTagsNotFound(locationId)
        val notFound = "Not Found( $notFoundCount" + ")"

        var differntLocationCount =
            bookDao.getCountFoundDifferentLoc(inventorymaster.scanID, locationId)
        val differentLocation = "Different Location $differntLocationCount"

        var countofNotRegistered = bookDao.getCountNotRegistered(inventorymaster.scanID)
        val notRegistered = "Not Registered $countofNotRegistered"




        tablayout.addTab(tablayout.newTab().setText(notFound));
        tablayout.addTab(tablayout.newTab().setText(differentLocation))
        tablayout.addTab(tablayout.newTab().setText(notRegistered))
        tablayout.tabGravity = TabLayout.GRAVITY_FILL

        viewPager.offscreenPageLimit = 3

        adapter = ReconcileAssetsPagerAdapter(this)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))

        buttonScan.setOnClickListener {
            if (viewPager.currentItem == 0) {

                lifecycleScope.launch {
                    val activity = activity as MainActivity?
                    activity?.inventoryStartOrStop()
                }
                addScan()

            } else if (viewPager.currentItem == 1) {
                if (adapter.getCurrentFragment() is DifferentLoactionFragment) {
                    val listBook = ArrayList<AssetCatalogue>()
                    (adapter.getCurrentFragment() as DifferentLoactionFragment).listBook.forEach {
                        if (it.assetCatalogue.isSelected) {
                            val assetCatalog = it.assetCatalogue
                            assetCatalog.locationId = locationId
                            listBook.add(assetCatalog)
                        }
                    }
                    if (listBook.isNotEmpty()) {
                        bookDao.updateBookAndAssetData(listBook)
                        (adapter.getCurrentFragment() as DifferentLoactionFragment).updateList()
                    } else
                        Toast.makeText(
                            requireContext(), "No Item Selected",
                            Toast.LENGTH_LONG
                        ).show()
                }

                var differntLocationCount =
                    bookDao.getCountFoundDifferentLoc(inventorymaster.scanID, locationId)
                val differentLocation = "Different Location $differntLocationCount"

                tablayout.getTabAt(1)?.text = differentLocation


            } else if (viewPager.currentItem == 2) {
//                Application.locateTag = inventorymaster.rfidTag
//                RFIDController.accessControlTag = ""
//                Application.PreFilterTag =""


                if (adapter.getCurrentFragment() is NotRegisteredFragment) {
                    val listRfids = ArrayList<ScanTag>()
                    (adapter.getCurrentFragment() as NotRegisteredFragment).rfidTags.forEach {
                        if (it.isSelected) {
                            listRfids.add(it)
                        }
                    }
                    if (listRfids.isNotEmpty()) {

                        replaceFragment(
                            requireActivity().supportFragmentManager, LocateOperationsFragment(),
                            R.id.content_frame
                        )


                    } else
                        Toast.makeText(
                            requireContext(), "Plese select item",
                            Toast.LENGTH_LONG
                        ).show()


                }


//                var countofNotRegistered = bookDao.getCountNotRegistered(inventorymaster.scanID)
//                val notRegistered = "Not Registered $countofNotRegistered"
//                tablayout.getTabAt(2)?.text = notRegistered


            }

        }

        tvUpdate.setOnClickListener {
            when (viewPager.currentItem) {
                0 -> {//faizan ne mana kiya
                }
                1 -> {
                    if (adapter.getCurrentFragment() is DifferentLoactionFragment) {
                        val listRfids = ArrayList<ScanTag>()

                        val scanList = bookDao.getScanTagAll()
                        val listBook =
                            (adapter.getCurrentFragment() as DifferentLoactionFragment).listBook

                        Log.e(
                            "sdd",
                            "" + Gson().toJson((adapter.getCurrentFragment() as DifferentLoactionFragment).listBook)
                        )

                        listBook.forEach {
                            if (it.assetCatalogue.isSelected) {
                                val assetCatalogue = it.assetCatalogue
                                scanList.forEachIndexed { index, scanTag ->
                                    if (scanTag.rfidTag == assetCatalogue.rfidTag) {
                                        listRfids.add(scanTag)
                                    }
                                }
                            }
                        }

                        if (listRfids.isNotEmpty()) {
                            bookDao.deleteScanTag(listRfids)
                            (adapter.getCurrentFragment() as DifferentLoactionFragment).updateList()
                        } else
                            Toast.makeText(
                                requireContext(), "No Item Selected",
                                Toast.LENGTH_LONG
                            ).show()


                        var countofNotRegistered =
                            bookDao.getCountNotRegistered(inventorymaster.scanID)
                        val notRegistered = "Different Location $countofNotRegistered"
                        tablayout.getTabAt(1)?.text = notRegistered
                    }
                }
                2 -> {
                    if (adapter.getCurrentFragment() is NotRegisteredFragment) {
                        val listRfids = ArrayList<ScanTag>()
                        (adapter.getCurrentFragment() as NotRegisteredFragment).rfidTags.forEach {
                            if (it.isSelected) {
                                listRfids.add(it)
                            }
                        }
                        if (listRfids.isNotEmpty()) {
                            bookDao.deleteScanTag(listRfids)
                            (adapter.getCurrentFragment() as NotRegisteredFragment).updateList()
                        } else
                            Toast.makeText(
                                requireContext(), "No Item Selected",
                                Toast.LENGTH_LONG
                            ).show()


                        var countofNotRegistered =
                            bookDao.getCountNotRegistered(inventorymaster.scanID)
                        val notRegistered = "Not Registered $countofNotRegistered"
                        tablayout.getTabAt(2)?.text = notRegistered


                    }
                }
            }
        }


        tablayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position

                if (viewPager.currentItem == 0) {
                    buttonScan.text = "Scan"
                    tvUpdate.text = "Update Location"

                } else if (viewPager.currentItem == 1) {
                    buttonScan.text = "Update to Current Location"
                    tvUpdate.text = "Moved to their Location"

                } else if (viewPager.currentItem == 2) {
                    buttonScan.text = "Search"
                    tvUpdate.text = "Ignore"

                }

            }


            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }


    fun addScan() {
        val inventoryMasterList = bookDao.getInventoryMaster()
        val inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]


        for (inventoryTag in listInventoryList) {
            val scanTag = ScanTag()
            scanTag.scanId = inventorymaster.scanID
            scanTag.locationId = locationId
            scanTag.rfidTag = inventoryTag
            val getCountOfTagAlready = bookDao.getCountOfTagAlready(
                scanTag.rfidTag!!, scanTag.scanId!!
            )
            if (getCountOfTagAlready == 0) {
                bookDao.addScanTag(scanTag)
                if (adapter.getCurrentFragment() is NotFoundFragment)
                    (adapter.getCurrentFragment() as NotFoundFragment).updateList()

                var notFoundCount = bookDao.getCountOfTagsNotFound(locationId)
                val notFound = "Not Found $notFoundCount"
                tablayout.getTabAt(0)?.text = notFound


            }
        }

    }

    override fun onResume() {
        super.onResume()
        adapter = ReconcileAssetsPagerAdapter(this)
        viewPager.adapter = adapter
    }


}